import datetime
import enum
from typing import TYPE_CHECKING, Optional

from slugify import UniqueSlugify
from sqlalchemy.event import listens_for
from sqlalchemy.orm import Mapped
from sqlalchemy.orm._orm_constructors import mapped_column

from shared import CommonModel, db

if TYPE_CHECKING:
    from .Submission import Submission


def unique_event_check(text, uids):
    if text in uids:
        return False
    return Event.find_by_slug(text) is None


event_slugify = UniqueSlugify(unique_check=unique_event_check, max_length=20)


class Event(db.Model, CommonModel):
    class Status(enum.Enum):
        open = "open"
        paired = "paired"
        finished = "finished"

        @property
        def displayname(self):
            return {
                self.open: "Submissions open",
                self.paired: "Paired",
                self.finished: "Finished",
            }[self]

    __tablename__ = "Event"

    title: Mapped[str] = mapped_column(
        db.String(length=200), unique=True, nullable=False
    )
    slug: Mapped[str] = mapped_column(db.String(length=20), unique=True, nullable=False)
    description: Mapped[str | None] = mapped_column(db.Text)
    status: Mapped[Status] = mapped_column(
        db.Enum(Status), server_default=Status.open.value, nullable=False
    )
    start_date: Mapped[datetime.date] = mapped_column(db.Date, nullable=False)
    deleted: Mapped[bool] = mapped_column(
        db.Boolean, nullable=False, server_default="0"
    )

    submission_deadline: Mapped[datetime.datetime | None] = mapped_column(
        db.DateTime, nullable=True
    )
    review_deadline: Mapped[datetime.datetime | None] = mapped_column(
        db.DateTime, nullable=True
    )
    submissions: Mapped[list["Submission"]] = db.relationship(
        "Submission", back_populates="event", cascade="all, delete"
    )

    @staticmethod
    def find_by_slug(slug: str) -> Optional["Event"]:
        return Event.query.filter_by(slug=slug).first()


@listens_for(Event, "before_insert")
def do_stuff(mapper, connect, target: Event):
    target.slug = event_slugify(target.title)
