from wtforms import Form, StringField
from wtforms.validators import DataRequired


class IdeaForm(Form):
    text = StringField("Idea", validators=[DataRequired()])
