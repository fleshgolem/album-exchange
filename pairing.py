from random import choice

from model.Submission import Submission
from shared import db


class PairingException(Exception):
    def __init__(self, message: str):
        self.message = message


def pair_submissions(input: list[Submission]):
    pairs = paired_submissions(input)

    for submission, recipient_submission in pairs:
        submission.receiver_id = recipient_submission.submitter_id
        submission.receiver_name = recipient_submission.submitter_name
        db.session.add(submission)
    db.session.commit()


def paired_submissions(
    input: list[Submission], iteration=0
) -> list[tuple[Submission, Submission]]:
    if iteration > 10:
        raise PairingException(
            "Tried 10 different pairings and could not reach a conclusion"
        )
    pairs: list[tuple[Submission, Submission]] = []
    recipients = [s for s in input]

    # Pick pairs
    for submission in input:
        r = choice(recipients)
        pairs.append((submission, r))
        recipients.remove(r)

    # verify no self-pairing
    if verify_no_self_pairings(pairs):
        return pairs
    else:
        return paired_submissions(input, iteration + 1)


def verify_no_self_pairings(pairs: list[tuple[Submission, Submission]]) -> bool:
    for p in pairs:
        if (
            p[0].submitter_id is not None and p[0].submitter_id == p[1].submitter_id
        ) or (
            p[0].submitter_name is not None
            and p[0].submitter_name == p[1].submitter_name
        ):
            return False
    return True
