from flask_wtf.file import FileAllowed
from wtforms import FileField, Form, StringField, TextAreaField
from wtforms.validators import DataRequired


class SubmissionForm(Form):
    title = StringField(
        "Title",
        validators=[DataRequired()],
        description='Please use "Artist - Title" formatting',
    )
    link = StringField(
        "Link",
        description="Please provide a link, where your album can be streamed legally",
    )
    secondary_link = StringField(
        "Secondary Link",
        description="Add a second link if necessary, e.g. for split releases",
    )
    blurb = TextAreaField(
        "Blurb", description="Any additional info, e.g. bonus tracks to ignore"
    )
    cover = FileField(
        "Cover Art",
        [FileAllowed(["jpg", "jpeg", "png"], "Images only!")],
        description="This is usually not required and you do not need to upload anything here unless the automated image grabber fails",
    )


class ReviewForm(Form):
    text = TextAreaField(
        "Text",
        description="Text supports Markdown (use highlights the same as you would in Discord)",
    )
