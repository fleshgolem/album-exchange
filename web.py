import os
from os import path
from uuid import uuid4

import PIL
from flask import (
    Blueprint,
    abort,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_discord import exceptions
from oauthlib.oauth2.rfc6749.errors import OAuth2Error
from PIL import Image
from sqlalchemy import func, select
from sqlalchemy.orm import joinedload
from werkzeug.datastructures import FileStorage

from forms.idea import IdeaForm
from forms.submission import ReviewForm, SubmissionForm
from model.DiscordUser import DiscordUser, current_user, local_user_from_discord
from model.Event import Event
from model.Idea import Idea, IdeaVote
from model.Submission import Submission
from shared import db

web = Blueprint("web", __name__)


@web.before_request
def require_auth():
    if not current_app.discord.authorized:
        raise exceptions.Unauthorized
    try:
        local_user_from_discord()
    except OAuth2Error as e:
        print(f"OAUTH ERROR: {e}")
        session.clear()
        flash("An error occured, please log in again", "is-danger")
        return redirect("login")


@web.route("/")
def index():
    current_events = (
        db.session.execute(
            db.select(Event)
            .filter(Event.deleted == False)
            .filter(
                (Event.status == Event.Status.open)
                | (Event.status == Event.Status.paired)
            )
            .order_by(Event.start_date.desc())
        )
        .scalars()
        .all()
    )
    past_events = (
        db.session.execute(
            db.select(Event)
            .filter(Event.deleted == False)
            .filter(Event.status == Event.Status.finished)
            .order_by(Event.start_date.desc())
        )
        .scalars()
        .all()
    )
    return render_template(
        "overview.html", past_events=past_events, current_events=current_events
    )


@web.route("/<string:slug>", methods=["GET", "POST"])
def event_page(slug):
    event = Event.find_by_slug(slug)
    if not event:
        abort(404)

    if event.status == Event.Status.open:
        return event_submission_page(event)
    if event.status == Event.Status.paired:
        return event_pairing_page(event)
    if event.status == Event.Status.finished:
        return event_archive_page(event)


def _upload_cover(file: FileStorage, submission: Submission):
    uploads_dir = current_app.config.get("UPLOAD_FOLDER")
    if existing := submission.cover_filename:
        existing_path = path.join(uploads_dir, existing)
        try:
            os.remove(existing_path)
        except Exception:
            print(f"Could not remove file {existing_path}")

    image = Image.open(file.stream)
    maxsize = (1024, 1024)
    image.thumbnail(maxsize)
    image = image.convert("RGB")
    new_filename = f"{uuid4()}.jpg"
    new_filepath = path.join(uploads_dir, new_filename)
    image.save(new_filepath)
    submission.cover_filename = new_filename
    submission.cover_url = f"/uploads/{new_filename}"


def event_submission_page(event: Event):
    if current_user().banned:
        return render_template("submission.html", event=event, banned=True)

    form = SubmissionForm(request.form)
    submission = Submission.query.filter_by(
        submitter_id=current_user().id, event=event
    ).first()
    if submission and request.method == "GET":
        form.title.data = submission.album_title
        form.link.data = submission.link
        form.secondary_link.data = submission.secondary_link
        form.blurb.data = submission.blurb

    if request.method == "POST":
        if form.validate():
            if not submission:
                submission = Submission()
                submission.event = event
            submission.update(form.title.data)
            submission.submitter_id = current_user().id

            submission.link = form.link.data
            submission.secondary_link = form.secondary_link.data
            submission.blurb = form.blurb.data

            cover: FileStorage | None = request.files.get("cover")
            if cover:
                _upload_cover(cover, submission)

            db.session.add(submission)
            db.session.commit()
            return redirect(url_for(".event_page", slug=event.slug))

        return render_template(
            "submission.html", event=event, form=form, submission=submission
        )
    else:
        return render_template(
            "submission.html", event=event, form=form, submission=submission
        )


def event_pairing_page(event: Event):
    submissions = (
        Submission.query.filter_by(event_id=event.id)
        .order_by(Submission.album_title)
        .all()
    )
    own_submission = Submission.query.filter_by(
        submitter_id=current_user().id, event=event
    ).first()
    album = None
    form = ReviewForm()
    if own_submission:
        album = Submission.query.filter_by(
            event_id=event.id, receiver_id=current_user().id
        ).first()
        if album:
            form.text.default = album.review
            form.process(request.form)
    return render_template(
        "pairing.html", event=event, submissions=submissions, album=album, form=form
    )


def event_archive_page(event: Event):
    submissions = (
        Submission.query.filter_by(event_id=event.id)
        .order_by(Submission.album_title)
        .all()
    )
    return render_template("archive.html", event=event, submissions=submissions)


@web.route("/<string:slug>/review", methods=["POST"])
def review_album(slug: str):
    event = Event.find_by_slug(slug)
    if not event:
        abort(404)
    submission = Submission.query.filter_by(
        receiver_id=current_user().id, event=event
    ).first()
    if not submission:
        flash(
            "You seem to not participate in this event. Please contact a mod to add your review"
        )
        return redirect(url_for(".event_page", slug=slug))
    form = ReviewForm(request.form)
    review = form.text.data
    lines = review.split("\n")
    lines = [line.strip() for line in lines]
    review = "\n".join(lines)
    submission.review = review
    db.session.add(submission)
    db.session.commit()
    flash("Review submitted", "is-success")
    return redirect(url_for(".event_page", slug=slug))


@web.route("/<string:slug>/review/<int:submission_id>", methods=["GET"])
def show_review(slug: str, submission_id: int):
    submission = Submission.get_by_id(submission_id)
    if not submission:
        abort(404)
    return render_template("archive.html", submission=submission)


@web.route("/ideas", methods=["GET", "POST"])
def ideas():
    ideaform = IdeaForm(request.form)
    user = current_user()

    if request.method == "POST":
        if ideaform.validate():
            idea = Idea()
            idea.text = ideaform.text.data
            idea.submitter_id = user.id
            db.session.add(idea)
            db.session.commit()
            flash(f'Idea "{idea.text}" submitted', "is-success")
            return redirect(url_for(".ideas"))
    open_ideas = db.session.execute(
        Idea.query_with_upvotes().filter(Idea.rejected == False)
    ).all()
    rejected_ideas = (
        db.session.execute(
            select(Idea).filter(Idea.rejected == True).order_by(Idea.text)
        )
        .scalars()
        .all()
    )
    user_votes = (
        db.session.execute(select(IdeaVote).filter_by(user=user, type=IdeaVote.Type.up))
        .scalars()
        .all()
    )
    user_vote_dict = {v.idea_id: 1 for v in user_votes}

    return render_template(
        "ideas.html",
        open_ideas=open_ideas,
        rejected_ideas=rejected_ideas,
        user_vote_dict=user_vote_dict,
        form=ideaform,
    )


@web.route("/ideas/<int:id>/vote/up", methods=["GET"])
def upvote_idea(id):
    user = current_user()
    idea = Idea.get_by_id(id)
    if idea:
        IdeaVote.create(user, idea, IdeaVote.Type.up)
        db.session.commit()
    return redirect(url_for(".ideas"))


@web.route("/ideas/<int:id>/vote/remove", methods=["GET"])
def remove_idea_vote(id):
    user = current_user()
    idea = Idea.get_by_id(id)
    if idea:
        IdeaVote.remove(user, idea, IdeaVote.Type.up)
        db.session.commit()

    return redirect(url_for(".ideas"))


@web.route("/users")
def user_list():
    submissions = func.count(Submission.id).label("submissions")
    q = (
        db.session.query(DiscordUser, submissions)
        .join(DiscordUser.submitted_albums)
        .having(submissions >= 3)
        .order_by(DiscordUser.name)
        .group_by(DiscordUser.id)
    )
    users = q.all()
    return render_template("user_list.html", users=users)


@web.route("/users/<int:id>")
def user_history(id):
    user = DiscordUser.get_by_id(id)
    if not user:
        abort(404)
    submissions = (
        Submission.query.options(joinedload(Submission.event))
        .join(Submission.event)
        .filter(Event.status == Event.Status.finished)
        .filter(Submission.submitter_id == id)
        .all()
    )
    reviewed = (
        Submission.query.options(joinedload(Submission.event))
        .filter(Submission.receiver_id == id)
        .filter(Submission.review != "")
        .all()
    )
    return render_template(
        "user_history.html", user=user, submissions=submissions, reviewed=reviewed
    )
