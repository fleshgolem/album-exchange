"""add cover filename to submission

Revision ID: e2a357d3b30b
Revises: 68992e8ed126
Create Date: 2023-03-03 20:45:31.401029

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e2a357d3b30b"
down_revision = "68992e8ed126"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "Submission", sa.Column("cover_filename", sa.String(length=150), nullable=True)
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("Submission", "cover_filename")
    # ### end Alembic commands ###
