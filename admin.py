from collections import defaultdict
from functools import wraps
from operator import or_

from flask import (
    Blueprint,
    abort,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_discord import exceptions
from oauthlib.oauth2.rfc6749.errors import OAuth2Error
from sqlalchemy import select
from sqlalchemy.orm import contains_eager

from forms import admin as adminforms
from model.DiscordUser import DiscordUser, Role, current_user, local_user_from_discord
from model.Event import Event
from model.Idea import Idea, IdeaVote
from model.Submission import Submission
from pairing import PairingException, pair_submissions
from shared import db

admin = Blueprint("admin", __name__, url_prefix="/admin")


@admin.before_request
def check_auth():
    if not current_app.discord.authorized:
        raise exceptions.Unauthorized
    try:
        local_user_from_discord()
    except OAuth2Error as e:
        print(f"OAUTH ERROR: {e}")
        session.clear()
        flash("An error occured, please log in again", "is-danger")
        return redirect("login")
    if not current_user().can_administer:
        abort(401)


def requires_full_admin(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not current_user().full_admin:
            abort(401)
        return func(*args, **kwargs)

    return wrapper


@admin.route("/")
def index():
    events = Event.query.order_by(Event.start_date.desc()).all()
    return render_template("admin/index.html", events=events)


@admin.route("/event", methods=["GET", "POST"])
def create_event():
    form = adminforms.EventForm(request.form)
    if request.method == "POST":
        if Event.query.filter_by(title=form.title.data).first():
            form.title.errors = ["Event with that title exists already"]
            return render_template("admin/create_event.html", form=form)
        elif form.validate():
            event = Event()
            event.title = form.title.data
            event.description = form.description.data
            event.start_date = form.start_date.data
            event.submission_deadline = form.submission_deadline.data
            event.review_deadline = form.review_deadline.data
            db.session.add(event)
            db.session.commit()
            flash("Event created", "is-success")
            return redirect(url_for(".index"))

        return render_template("admin/create_event.html", form=form)
    else:
        return render_template("admin/create_event.html", form=form)


@admin.route("/event/<string:slug>", methods=["GET"])
def show_event(slug):
    event = Event.find_by_slug(slug)
    submissions = (
        Submission.query.filter_by(event_id=event.id)
        .order_by(Submission.album_title)
        .all()
    )
    if not event:
        abort(404)
    return render_template("admin/event.html", event=event, submissions=submissions)


@admin.route("/event/<string:slug>/delete", methods=["POST"])
@requires_full_admin
def delete_event(slug):
    event = Event.find_by_slug(slug)
    if not event:
        abort(404)
    event.deleted = True
    db.session.add(event)
    db.session.commit()
    flash("Event deleted", "is-success")
    return redirect(url_for(".index"))


@admin.route("/event/<string:slug>/edit", methods=["GET", "POST"])
def edit_event(slug):
    event = Event.find_by_slug(slug)
    if not event:
        abort(404)
    form = adminforms.EventForm()
    form.title.default = event.title
    form.description.default = event.description
    form.start_date.default = event.start_date
    form.status.default = event.status.value
    form.submission_deadline.default = event.submission_deadline
    form.review_deadline.default = event.review_deadline
    form.process(request.form)
    if request.method == "POST":
        if form.validate():
            event.title = form.title.data
            event.description = form.description.data
            event.start_date = form.start_date.data
            event.status = form.status.data
            event.submission_deadline = form.submission_deadline.data
            event.review_deadline = form.review_deadline.data
            db.session.add(event)
            db.session.commit()
            flash("Event created", "is-success")
            return redirect(url_for(".show_event", slug=slug))

        return render_template("admin/create_event.html", form=form, event=event)
    else:
        return render_template("admin/create_event.html", form=form, event=event)


@admin.route("/event/<string:slug>/pair", methods=["POST"])
def pair_event(slug):
    event = Event.find_by_slug(slug)
    if not event:
        abort(404)
    try:
        pair_submissions(event.submissions)
        event.status = Event.Status.paired.value
        db.session.add(event)
        db.session.commit()
        flash("Event paired", "is-success")
    except PairingException as e:
        flash(f"Could not pair event: {e.message}", "is-danger")
    return redirect(url_for(".show_event", slug=slug))


@admin.route("/event/<string:slug>/finish", methods=["POST"])
def finish_event(slug):
    event = Event.find_by_slug(slug)
    if not event:
        abort(404)
    event.status = Event.Status.finished.value
    db.session.add(event)
    db.session.commit()
    flash("Event finished", "is-success")
    return redirect(url_for(".show_event", slug=slug))


@admin.route(
    "/event/<string:event_slug>/submission/<int:id>/edit", methods=["GET", "POST"]
)
def edit_submission(event_slug: str, id: int):
    submission = Submission.get_by_id(id)
    if not submission:
        abort(404)
    form = adminforms.SubmissionForm()
    users = DiscordUser.query.order_by(DiscordUser.name).all()
    user_pairs = [(None, "")]
    user_pairs.extend([(u.id, f"{u.name}#{u.discriminator}") for u in users])
    print(user_pairs)
    form.submitter.choices = user_pairs
    form.recipient.choices = user_pairs
    form.title.default = submission.album_title
    form.link.default = submission.link
    form.secondary_link.default = submission.secondary_link
    form.submitter.default = submission.submitter_id
    form.recipient.default = submission.receiver_id
    form.review.default = submission.review
    form.force_cover_url.default = submission.force_cover_url
    form.blurb.default = submission.blurb
    form.process(request.form)
    if request.method == "POST":
        if form.validate():
            submission.update(form.title.data)
            submission.link = form.link.data
            submission.secondary_link = form.secondary_link.data
            submission.submitter_id = form.submitter.data
            if form.recipient.data == "None":
                submission.receiver_id = None
            elif form.recipient.data:
                submission.receiver_id = form.recipient.data
            submission.review = form.review.data
            submission.force_cover_url = form.force_cover_url.data
            if submission.force_cover_url:
                submission.cover_url = submission.force_cover_url
            submission.blurb = form.blurb.data
            db.session.add(submission)
            db.session.commit()
            flash("Submission updated", "is-success")
            return redirect(url_for(".show_event", slug=submission.event.slug))

        return render_template(
            "admin/edit_submission.html", form=form, submission=submission
        )
    else:
        return render_template(
            "admin/edit_submission.html", form=form, submission=submission
        )


@admin.route("/event/<string:event_slug>/submission/add", methods=["GET", "POST"])
def add_submission(event_slug):
    event = Event.find_by_slug(event_slug)
    if not event:
        abort(404)

    form = adminforms.SubmissionForm(request.form)
    users = DiscordUser.query.order_by(DiscordUser.name).all()
    user_pairs = [(None, "")]
    user_pairs.extend([(u.id, f"{u.name}#{u.discriminator}") for u in users])
    form.submitter.choices = user_pairs
    form.recipient.choices = user_pairs

    form.process(request.form)
    if request.method == "POST":
        if form.validate():
            submission = Submission()
            submission.event = event
            submission.update(form.title.data)
            submission.link = form.link.data
            submission.submitter_id = form.submitter.data
            if form.recipient.data == "None":
                submission.receiver_id = None
            elif form.recipient.data:
                submission.receiver_id = form.recipient.data
            submission.review = form.review.data
            db.session.add(submission)
            db.session.commit()
            flash("Submission added", "is-success")
            return redirect(url_for(".show_event", slug=submission.event.slug))

        return render_template("admin/edit_submission.html", form=form)
    else:
        return render_template("admin/edit_submission.html", form=form)


@admin.route("/submission/delete", methods=["POST"])
def delete_submission():
    print(request.form)
    id = request.form.get("id")
    submission = Submission.get_by_id(id)
    if not submission:
        abort(404)
    event = submission.event
    db.session.delete(submission)
    db.session.commit()
    flash("Submission Deleted", "is-success")
    return redirect(url_for(".show_event", slug=event.slug))


@admin.route("/shitlist", methods=["GET"])
def shitlist():
    qry = (
        db.session.query(Event)
        .join(Event.submissions)
        .filter(or_(Submission.review == "", Submission.review == None))
        .filter(Event.deleted == False)
        .filter(Event.status != Event.Status.open)
        .order_by(Event.start_date.desc())
        .options(contains_eager(Event.submissions))
    )
    print(qry)
    results = qry.all()

    return render_template("admin/shitlist.html", shitlist=results)


@admin.route("/ideas", methods=["GET"])
@requires_full_admin
def ideas():
    open_ideas = db.session.execute(
        Idea.query_with_upvotes().filter(Idea.rejected == False)
    ).all()
    rejected_ideas = (
        db.session.execute(
            select(Idea).filter(Idea.rejected == True).order_by(Idea.text)
        )
        .scalars()
        .all()
    )
    rejection_votes = (
        db.session.execute(select(IdeaVote.query.filter_by(type=IdeaVote.Type.reject)))
        .scalars()
        .all()
    )
    rejection_vote_counts = defaultdict(int)
    rejection_vote_users = defaultdict(list)
    for v in rejection_votes:
        rejection_vote_users[v.idea_id].append(v.user.name)
        rejection_vote_counts[v.idea_id] += 1
    rejection_vote_users = {
        idea: "&#10;".join(usernames)
        for (idea, usernames) in rejection_vote_users.items()
    }

    user = current_user()
    user_rejection_votes = (
        db.session.execute(
            select(IdeaVote).filter_by(user=user, type=IdeaVote.Type.reject)
        )
        .scalars()
        .all()
    )
    user_rejection_vote_dict = {v.idea_id: 1 for v in user_rejection_votes}
    return render_template(
        "admin/ideas.html",
        open_ideas=open_ideas,
        rejected_ideas=rejected_ideas,
        rejection_vote_counts=rejection_vote_counts,
        rejection_vote_users=rejection_vote_users,
        user_rejection_vote_dict=user_rejection_vote_dict,
    )


@admin.route("/ideas/<int:id>/reject")
@requires_full_admin
def reject_idea(id):
    idea = Idea.get_by_id(id)
    if not idea:
        abort(404)
    idea.rejected = True
    db.session.add(idea)
    db.session.commit()
    return redirect(url_for(".ideas"))


@admin.route("/ideas/<int:id>/unreject")
@requires_full_admin
def unreject_idea(id):
    idea = Idea.get_by_id(id)
    if not idea:
        abort(404)
    idea.rejected = False
    db.session.add(idea)
    db.session.commit()
    return redirect(url_for(".ideas"))


@admin.route("/ideas/<int:id>/vote")
@requires_full_admin
def vote_reject_idea(id):
    idea = Idea.get_by_id(id)
    user = current_user()
    if not idea:
        abort(404)
    IdeaVote.create(user, idea, IdeaVote.Type.reject)
    db.session.commit()
    return redirect(url_for(".ideas"))


@admin.route("/ideas/<int:id>/vote/remove")
@requires_full_admin
def remove_vote_reject_idea(id):
    idea = Idea.get_by_id(id)
    user = current_user()
    if not idea:
        abort(404)
    IdeaVote.remove(user, idea, IdeaVote.Type.reject)
    db.session.commit()
    return redirect(url_for(".ideas"))


@admin.route("/ideas/delete", methods=["POST"])
@requires_full_admin
def delete_idea():
    id = request.form.get("id")
    idea = Idea.get_by_id(id)
    if not idea:
        abort(404)
    db.session.delete(idea)
    db.session.commit()
    flash("Submission Deleted", "is-success")
    return redirect(url_for(".ideas"))


@admin.route("/users")
@requires_full_admin
def users():
    users = DiscordUser.query.order_by(DiscordUser.name).all()
    return render_template("admin/users.html", users=users)


@admin.route("/users/<int:id>/add_admin")
@requires_full_admin
def add_admin(id):
    user = DiscordUser.get_by_id(id)
    user.role = Role.admin
    db.session.add(user)
    db.session.commit()
    return redirect(url_for(".users"))


@admin.route("/users/<int:id>/add_runner")
@requires_full_admin
def add_runner(id):
    user = DiscordUser.get_by_id(id)
    user.role = Role.runner
    db.session.add(user)
    db.session.commit()
    return redirect(url_for(".users"))


@admin.route("/users/<int:id>/remove_roles")
@requires_full_admin
def remove_roles(id):
    user = DiscordUser.get_by_id(id)
    user.role = Role.user
    db.session.add(user)
    db.session.commit()
    return redirect(url_for(".users"))


@admin.route("/users/<int:id>/toggle_ban")
@requires_full_admin
def toggle_ban(id):
    enable = "enable" in request.args
    user = DiscordUser.get_by_id(id)
    user.banned = enable
    db.session.add(user)
    db.session.commit()
    return redirect(url_for(".users"))
