FROM tiangolo/uwsgi-nginx-flask:python3.11
ENV FLASK_APP="main:app"
RUN pip install poetry
COPY poetry.lock .
COPY pyproject.toml .
RUN poetry config virtualenvs.create false; poetry env info
RUN poetry install --no-root --no-dev
COPY . .