import enum
from typing import TYPE_CHECKING

from flask import _request_ctx_stack, g, has_request_context
from sqlalchemy.orm import Mapped
from sqlalchemy.orm._orm_constructors import mapped_column

from model.Submission import Submission
from shared import CommonModel, db, discord

if TYPE_CHECKING:
    from .Idea import IdeaVote


class Role(enum.Enum):
    user = "user"
    runner = "runner"
    admin = "admin"


class DiscordUser(db.Model, CommonModel):
    __tablename__ = "DiscordUser"
    name: Mapped[str] = mapped_column(db.String(100), nullable=False, index=True)
    discriminator: Mapped[int] = mapped_column(db.Integer, nullable=False)
    avatar_url: Mapped[str | None] = mapped_column(db.Text)
    discord_id: Mapped[int] = mapped_column(db.BigInteger, nullable=False, index=True)
    banned: Mapped[bool] = mapped_column(db.Boolean, nullable=False, server_default="0")
    role: Mapped[Role] = mapped_column(db.Enum(Role), server_default=Role.user.value)

    submitted_albums: Mapped[list["Submission"]] = db.relationship(
        "Submission", back_populates="submitter", foreign_keys=[Submission.submitter_id]
    )
    reviewed_albums: Mapped[list["Submission"]] = db.relationship(
        "Submission", back_populates="receiver", foreign_keys=[Submission.receiver_id]
    )
    votes: Mapped[list["IdeaVote"]] = db.relationship(
        "IdeaVote", back_populates="user", cascade="all, delete"
    )

    @property
    def can_administer(self):
        return self.role in [Role.admin, Role.runner]

    @property
    def full_admin(self):
        return self.role == Role.admin

    @staticmethod
    def from_discord(user) -> "DiscordUser":
        duser = DiscordUser.query.filter_by(discord_id=user.id).first()
        if not duser:
            duser = DiscordUser()
            duser.discord_id = user.id
        duser.name = user.name
        duser.discriminator = user.discriminator
        duser.avatar_url = user.avatar_url
        db.session.add(duser)
        db.session.commit()
        return duser

    @staticmethod
    def logout():
        discord.revoke()
        g.current_user = None


def local_user_from_discord():
    user = discord.fetch_user()
    duser = DiscordUser.from_discord(user)
    db.session.add(duser)
    db.session.commit()
    _request_ctx_stack.top.user = duser


def current_user() -> DiscordUser | None:
    if has_request_context() and not hasattr(_request_ctx_stack.top, "user"):
        return None
    return getattr(_request_ctx_stack.top, "user", None)
