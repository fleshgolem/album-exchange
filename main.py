import logging
from datetime import timedelta
from logging.handlers import RotatingFileHandler
from uuid import uuid4

from flask import (
    Flask,
    redirect,
    render_template,
    request,
    send_from_directory,
    session,
    url_for,
)
from flask_discord import Unauthorized
from flaskext.markdown import Markdown

from admin import admin
from model.DiscordUser import DiscordUser, current_user
from shared import db, discord, migrate
from web import web

app = Flask(__name__)
app.config.from_pyfile("config.py")
# This should hopefully fix the issue with inactive connections causing 500 errors
app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {"pool_size": 10, "pool_recycle": 280}
Markdown(app, extensions=["nl2br", "pymdownx.tilde", "fenced_code"])
db.init_app(app)
discord.init_app(app)
migrate.init_app(app, db)

app.register_blueprint(admin)
app.register_blueprint(web)


@app.context_processor
def inject_user():
    return dict(current_user=current_user())


@app.route("/uploads/<string:filename>")
def upload(filename: str):
    return send_from_directory(app.config["UPLOAD_FOLDER"], filename)


@app.route("/login/", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        return discord.create_session(scope=["identify"])
    return render_template("login.html")


@app.route("/callback/")
def callback():
    discord.callback()
    user = discord.fetch_user()
    duser = DiscordUser.from_discord(user)
    db.session.add(duser)
    db.session.commit()
    return redirect(url_for("web.index"))


@app.route("/logout/")
def logout():
    DiscordUser.logout()
    return redirect(url_for(".login"))


@app.errorhandler(Unauthorized)
def redirect_unauthorized(e):
    return redirect(url_for("login"))


@app.route("/robots.txt")
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(days=1000)

    if "uuid" not in session:
        session["uuid"] = uuid4().hex


file_handler = RotatingFileHandler("error.log", maxBytes=5000)
file_handler.setLevel(logging.WARN)
file_handler.setFormatter(
    logging.Formatter("[%(asctime)s] %(levelname)s in %(module)s: %(message)s")
)

if not app.debug:
    app.logger.addHandler(file_handler)

if __name__ == "__main__":
    app.run(debug=False)
