"""add rejection votes

Revision ID: 6e7634585fbf
Revises: d3d8d5ca2c6d
Create Date: 2020-11-01 22:04:10.485954

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6e7634585fbf"
down_revision = "d3d8d5ca2c6d"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "idea",
        sa.Column("rejection_votes", sa.Integer(), server_default="0", nullable=True),
    )
    op.create_index(
        op.f("ix_idea_rejection_votes"), "idea", ["rejection_votes"], unique=False
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f("ix_idea_rejection_votes"), table_name="idea")
    op.drop_column("idea", "rejection_votes")
    # ### end Alembic commands ###
