from typing import TypeVar

from cachetools import TTLCache
from flask_discord import DiscordOAuth2Session
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import select
from sqlalchemy.orm import Mapped
from sqlalchemy.orm._orm_constructors import mapped_column

db = SQLAlchemy()
migrate = Migrate()
T = TypeVar("T", bound="CommonModel")
discord = DiscordOAuth2Session(users_cache=TTLCache(maxsize=100, ttl=300))


class CommonModel:
    id: Mapped[int] = mapped_column(db.Integer, primary_key=True)

    @classmethod
    def get_by_id(cls: T, id: int) -> T | None:
        return db.session.execute(select(cls).filter_by(id=id)).scalars().first()
