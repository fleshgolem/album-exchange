from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped
from sqlalchemy.orm._orm_constructors import mapped_column

from lastfm import find_album_cover
from shared import CommonModel, db

if TYPE_CHECKING:
    from .DiscordUser import DiscordUser
    from .Event import Event


class Submission(db.Model, CommonModel):
    __tablename__ = "Submission"
    album_title: Mapped[str | None] = mapped_column(db.Text)

    submitter_name: Mapped[str | None] = mapped_column(
        db.String(length=200), nullable=True, index=True
    )
    receiver_name: Mapped[str | None] = mapped_column(
        db.String(length=200), nullable=True, index=True
    )

    submitter_session: Mapped[str | None] = mapped_column(
        db.String(length=32), nullable=True, index=True
    )
    receiver_session: Mapped[str | None] = mapped_column(
        db.String(length=32), nullable=True, index=True
    )

    submitter_id: Mapped[int | None] = mapped_column(
        db.Integer, db.ForeignKey("DiscordUser.id"), nullable=True
    )

    receiver_id: Mapped[int | None] = mapped_column(
        db.Integer, db.ForeignKey("DiscordUser.id"), nullable=True
    )

    event_id: Mapped[int] = mapped_column(
        db.Integer, db.ForeignKey("Event.id"), nullable=False
    )

    cover_url: Mapped[str | None] = mapped_column(db.Text)
    cover_filename: Mapped[str | None] = mapped_column(db.String(150), nullable=True)
    link: Mapped[str | None] = mapped_column(db.Text)
    secondary_link: Mapped[str | None] = mapped_column(db.Text)
    review: Mapped[str | None] = mapped_column(db.Text)
    blurb: Mapped[str | None] = mapped_column(db.Text)

    force_cover_url: Mapped[str | None] = mapped_column(db.Text)

    submitter: Mapped["DiscordUser"] = db.relationship(
        "DiscordUser", foreign_keys=[submitter_id]
    )
    receiver: Mapped["DiscordUser"] = db.relationship(
        "DiscordUser", foreign_keys=[receiver_id]
    )
    event: Mapped["Event"] = db.relationship("Event", foreign_keys=[event_id])

    def update(self, album_title):
        self.album_title = album_title
        self.cover_url = find_album_cover(self.album_title)
