"""add vote ideas

Revision ID: 13820c21c57a
Revises: 2515d88190ad
Create Date: 2020-12-08 12:39:07.455991

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "13820c21c57a"
down_revision = "2515d88190ad"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "IdeaVote",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("idea_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column(
            "type",
            sa.Enum("up", "reject", name="type"),
            server_default="up",
            nullable=False,
        ),
        sa.ForeignKeyConstraint(
            ["idea_id"],
            ["idea.id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["DiscordUser.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("IdeaVote")
    # ### end Alembic commands ###
