from datetime import date, datetime, timedelta

from wtforms import (
    DateField,
    DateTimeField,
    Form,
    SelectField,
    StringField,
    TextAreaField,
)
from wtforms.validators import DataRequired, Length

from model.Event import Event


class EventForm(Form):
    title = StringField("Title", validators=[DataRequired(), Length(max=200)])
    description = TextAreaField("Description", description="Supports Markdown")
    start_date = DateField("Start Date", default=lambda: date.today())
    status = SelectField(
        "Status",
        choices=[(e.value, e.displayname) for e in list(Event.Status)],
        default=Event.Status.open.value,
    )
    submission_deadline = DateTimeField(
        "Submission Deadline",
        description='Format YYYY-MM-DD HH:MM. Provide time as <a href="https://time.is/de/UTC">UTC</a>',
        format="%Y-%m-%d %H:%M",
        default=lambda: datetime.utcnow() + timedelta(days=1),
    )
    review_deadline = DateTimeField(
        "Review Deadline",
        description='Format YYYY-MM-DD HH:MM. Provide time as <a href="https://time.is/de/UTC">UTC</a>',
        format="%Y-%m-%d %H:%M",
        default=lambda: datetime.utcnow() + timedelta(days=7),
    )


class SubmissionForm(Form):
    title = StringField("Title", validators=[DataRequired()])
    link = StringField("Link")
    secondary_link = StringField("Secondary Link")
    submitter = SelectField("Submitter")
    recipient = SelectField("Recipient")
    review = TextAreaField("Review")
    blurb = TextAreaField(
        "Blurb", description="Any additional info, e.g. bonus tracks to ignore"
    )
    force_cover_url = StringField(
        "Force cover URL",
        description="Manually provide a cover URL to an album. This should not be necessary, "
        "unless last.fm screws up royally",
    )
