# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = album_results_from_dict(json.loads(json_string))

from collections.abc import Callable
from dataclasses import dataclass
from enum import Enum
from typing import Any, TypeVar, cast

import requests
from flask import current_app

T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def to_enum(c: type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


def from_list(f: Callable[[Any], T], x: Any) -> list[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c: type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


class Artist(Enum):
    ARTIST_CONVERGE = '"Converge"'
    CONVERGE = "Converge"


class Size(Enum):
    EXTRALARGE = "extralarge"
    LARGE = "large"
    MEDIUM = "medium"
    SMALL = "small"


@dataclass
class Image:
    text: str
    size: Size

    @staticmethod
    def from_dict(obj: Any) -> "Image":
        assert isinstance(obj, dict)
        text = from_str(obj.get("#text"))
        size = Size(obj.get("size"))
        return Image(text, size)

    def to_dict(self) -> dict:
        result: dict = {}
        result["#text"] = from_str(self.text)
        result["size"] = to_enum(Size, self.size)
        return result


@dataclass
class Album:
    name: str
    artist: Artist
    url: str
    image: list[Image]
    streamable: int
    mbid: str

    @staticmethod
    def from_dict(obj: Any) -> "Album":
        assert isinstance(obj, dict)
        name = from_str(obj.get("name"))
        artist = from_str(obj.get("artist"))
        url = from_str(obj.get("url"))
        image = from_list(Image.from_dict, obj.get("image"))
        streamable = int(from_str(obj.get("streamable")))
        mbid = from_str(obj.get("mbid"))
        return Album(name, artist, url, image, streamable, mbid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = from_str(self.name)
        result["artist"] = to_enum(Artist, self.artist)
        result["url"] = from_str(self.url)
        result["image"] = from_list(lambda x: to_class(Image, x), self.image)
        result["streamable"] = from_str(str(self.streamable))
        result["mbid"] = from_str(self.mbid)
        return result


@dataclass
class Albummatches:
    album: list[Album]

    @staticmethod
    def from_dict(obj: Any) -> "Albummatches":
        assert isinstance(obj, dict)
        album = from_list(Album.from_dict, obj.get("album"))
        return Albummatches(album)

    def to_dict(self) -> dict:
        result: dict = {}
        result["album"] = from_list(lambda x: to_class(Album, x), self.album)
        return result


@dataclass
class Attr:
    attr_for: str

    @staticmethod
    def from_dict(obj: Any) -> "Attr":
        assert isinstance(obj, dict)
        attr_for = from_str(obj.get("for"))
        return Attr(attr_for)

    def to_dict(self) -> dict:
        result: dict = {}
        result["for"] = from_str(self.attr_for)
        return result


@dataclass
class OpensearchQuery:
    text: str
    role: str
    search_terms: str
    start_page: int

    @staticmethod
    def from_dict(obj: Any) -> "OpensearchQuery":
        assert isinstance(obj, dict)
        text = from_str(obj.get("#text"))
        role = from_str(obj.get("role"))
        search_terms = from_str(obj.get("searchTerms"))
        start_page = int(from_str(obj.get("startPage")))
        return OpensearchQuery(text, role, search_terms, start_page)

    def to_dict(self) -> dict:
        result: dict = {}
        result["#text"] = from_str(self.text)
        result["role"] = from_str(self.role)
        result["searchTerms"] = from_str(self.search_terms)
        result["startPage"] = from_str(str(self.start_page))
        return result


@dataclass
class Results:
    opensearch_query: OpensearchQuery
    opensearch_total_results: int
    opensearch_start_index: int
    opensearch_items_per_page: int
    albummatches: Albummatches
    attr: Attr

    @staticmethod
    def from_dict(obj: Any) -> "Results":
        assert isinstance(obj, dict)
        opensearch_query = OpensearchQuery.from_dict(obj.get("opensearch:Query"))
        opensearch_total_results = int(from_str(obj.get("opensearch:totalResults")))
        opensearch_start_index = int(from_str(obj.get("opensearch:startIndex")))
        opensearch_items_per_page = int(from_str(obj.get("opensearch:itemsPerPage")))
        albummatches = Albummatches.from_dict(obj.get("albummatches"))
        attr = Attr.from_dict(obj.get("@attr"))
        return Results(
            opensearch_query,
            opensearch_total_results,
            opensearch_start_index,
            opensearch_items_per_page,
            albummatches,
            attr,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["opensearch:Query"] = to_class(OpensearchQuery, self.opensearch_query)
        result["opensearch:totalResults"] = from_str(str(self.opensearch_total_results))
        result["opensearch:startIndex"] = from_str(str(self.opensearch_start_index))
        result["opensearch:itemsPerPage"] = from_str(
            str(self.opensearch_items_per_page)
        )
        result["albummatches"] = to_class(Albummatches, self.albummatches)
        result["@attr"] = to_class(Attr, self.attr)
        return result


@dataclass
class AlbumResults:
    results: Results

    @staticmethod
    def from_dict(obj: Any) -> "AlbumResults":
        assert isinstance(obj, dict)
        results = Results.from_dict(obj.get("results"))
        return AlbumResults(results)

    def to_dict(self) -> dict:
        result: dict = {}
        result["results"] = to_class(Results, self.results)
        return result


def album_results_from_dict(s: Any) -> AlbumResults:
    return AlbumResults.from_dict(s)


def album_results_to_dict(x: AlbumResults) -> Any:
    return to_class(AlbumResults, x)


def find_album_cover(title: str) -> str | None:
    try:
        url = "https://ws.audioscrobbler.com/2.0/"
        args = {
            "method": "album.search",
            "api_key": current_app.config.get("LASTFM_API_KEY", ""),
            "format": "json",
            "album": title,
        }
        r = requests.get(url, params=args)
        print(r.text)
        if r.status_code == 200:
            res = album_results_from_dict(r.json())
            if len(res.results.albummatches.album) > 0:
                album = res.results.albummatches.album[0]
                if album.image and len(album.image) > 3:
                    return album.image[3].text
        return None
    except Exception as e:
        print(f"Lastfm error {e}")
        return None
