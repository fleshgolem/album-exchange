import enum

from sqlalchemy import and_, func, select, text
from sqlalchemy.orm import Mapped
from sqlalchemy.orm._orm_constructors import mapped_column

from model.DiscordUser import DiscordUser
from shared import CommonModel, db


class Idea(db.Model, CommonModel):
    text: Mapped[str | None] = mapped_column(db.Text)
    rejected: Mapped[bool] = mapped_column(db.Boolean, index=True, server_default="0")
    submitter_id: Mapped[int] = mapped_column(
        db.Integer, db.ForeignKey("DiscordUser.id"), nullable=True
    )

    submitter: Mapped[DiscordUser] = db.relationship(
        "DiscordUser", foreign_keys=[submitter_id]
    )
    votes: Mapped[list["IdeaVote"]] = db.relationship(
        "IdeaVote", back_populates="idea", cascade="all, delete"
    )

    @staticmethod
    def query_with_upvotes():
        return (
            select(Idea, func.count(Idea.votes).label("total"))
            .outerjoin(
                IdeaVote,
                and_(IdeaVote.idea_id == Idea.id, IdeaVote.type == IdeaVote.Type.up),
            )
            .group_by(Idea.id)
            .order_by(text("total DESC"), Idea.text)
        )


class IdeaVote(db.Model, CommonModel):
    __tablename__ = "IdeaVote"

    class Type(enum.Enum):
        up = "up"
        reject = "reject"

    idea_id: Mapped[int] = mapped_column(
        db.Integer, db.ForeignKey("idea.id"), nullable=False
    )
    user_id: Mapped[int] = mapped_column(
        db.Integer, db.ForeignKey("DiscordUser.id"), nullable=False
    )
    type: Mapped[Type] = mapped_column(
        db.Enum(Type), server_default=Type.up.value, nullable=False
    )

    idea: Mapped[Idea] = db.relationship(
        "Idea",
        foreign_keys=[idea_id],
    )
    user: Mapped["DiscordUser"] = db.relationship(
        "DiscordUser",
        foreign_keys=[user_id],
    )

    @staticmethod
    def create(user: DiscordUser, idea: Idea, type=Type.up) -> "IdeaVote":
        existing = IdeaVote.query.filter_by(user=user, idea=idea, type=type).first()
        if existing:
            return existing
        vote = IdeaVote()
        vote.user_id = user.id
        vote.idea_id = idea.id
        vote.type = type
        db.session.add(vote)
        return vote

    @staticmethod
    def remove(user: DiscordUser, idea: Idea, type=Type.up):
        existing = IdeaVote.query.filter_by(user=user, idea=idea, type=type).first()
        if existing:
            db.session.delete(existing)
